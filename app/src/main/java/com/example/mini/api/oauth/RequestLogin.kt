package com.example.mini.api.model.request

import com.google.gson.annotations.SerializedName

class RequestLogin (
    @SerializedName("email")
    val email: String,
    @SerializedName("password")
    val password: String
)