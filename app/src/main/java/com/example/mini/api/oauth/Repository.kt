package com.example.mini.api.repository

import android.util.Log
import com.example.mini.api.model.request.RequestLogin
import com.example.mini.api.RetrofitInstance
import com.example.mini.api.repository.listener.CallbackApi
import okhttp3.ResponseBody
import retrofit2.Call

import retrofit2.Callback
import retrofit2.Response

class Repository {

    fun login(email: String, password: String, listener: CallbackApi){
        val call = RetrofitInstance.api.login(request = RequestLogin(
            email = email,
            password = password
        )
        )
        call.enqueue(object : Callback<ResponseBody>{
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG", "onFailure: teste")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
//               (UID, model.headers()["uid"].toString())
//               (CLIENT, model.headers()["client"].toString())
//                (ACCESS_TOKEN, model.headers()["access-token"].toString())
//               (EMAIL, email)
//                (PASSWORD, password)
                if (response.isSuccessful){
                    listener.onSuccess(response.headers()["uid"].toString()+"\n "+response.headers()["access-token"].toString())
                }else{
                    listener.onFailure(response.body())
                }
            }
        })
    }
}