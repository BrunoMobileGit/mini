package com.example.mini.api.repository.listener

import okhttp3.ResponseBody

interface CallbackApi {
    fun onSuccess(item: String)
    fun onFailure(item: ResponseBody?)
}