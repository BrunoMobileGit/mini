package com.example.mini.api

import com.example.mini.api.model.request.RequestLogin
import com.example.mini.util.Constants
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface SimpleApi {

    //Endpoint
    @POST(Constants.LOGIN)
    fun login(@Body request: RequestLogin): Call<ResponseBody>



}