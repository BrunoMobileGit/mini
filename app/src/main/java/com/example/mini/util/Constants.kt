package com.example.mini.util

object Constants{

    const val BASE_URL = "https://empresas.ioasys.com.br/api/v1/"
    const val LOGIN = "users/auth/sign_in"
    const val UID = "AA-A"
    const val CLIENT = "AA-B"
    const val ACCESS_TOKEN = "AA-C"
}