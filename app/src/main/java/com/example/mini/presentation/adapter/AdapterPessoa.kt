package com.example.mini.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.example.mini.R
import com.example.mini.api.model.Pessoa
import kotlinx.android.synthetic.main.custom_adapter_pessoa.view.*
import java.util.*

class AdapterPessoa:RecyclerView.Adapter<AdapterPessoa.MyViewHolder>(), Filterable {

    var list = mutableListOf<Pessoa>()

    init {
        val p1 =
            Pessoa("Bruno Freitas", 24, "Aracaju")
        list.add(p1)
        val p2 =
            Pessoa("Marilia Anonima", 20, "Capela")
        list.add(p2)
    }

    var listFilter = list

    inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterPessoa.MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_adapter_pessoa,parent,false))
    }

    override fun getItemCount(): Int {
        return listFilter.size
    }

    override fun onBindViewHolder(holder: AdapterPessoa.MyViewHolder, position: Int) {
        val currentItem = listFilter[position]
       holder.itemView.nome_pessoa.text = currentItem.nome.toString()
       holder.itemView.idade_pessoa.text = currentItem.idade.toString()
       holder.itemView.cidade_pessoa.text = currentItem.cidade.toString()
    }

    override fun getFilter(): Filter {
       return object : Filter() {
           override fun performFiltering(p0: CharSequence?): FilterResults {
               val charSearch = p0.toString()
               if (charSearch.isEmpty()) {
                   listFilter = list
               } else {
                   val resultList = mutableListOf<Pessoa>()
                   for (row in list) {
                       if (row.nome.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))) {
                           resultList.add(row)
                       }
                   }
                   listFilter = resultList
               }
               val filterResults = FilterResults()
               filterResults.values = listFilter
               return filterResults
           }

           override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
               listFilter = p1?.values as MutableList<Pessoa>
               notifyDataSetChanged()
           }

       }
    }

}