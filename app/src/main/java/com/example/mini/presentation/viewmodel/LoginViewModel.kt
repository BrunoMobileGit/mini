package com.example.mini.presentation.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.example.mini.api.repository.Repository
import com.example.mini.api.repository.listener.CallbackApi
import okhttp3.ResponseBody

class LoginViewModel(application: Application): AndroidViewModel(application) {
    private val repository = Repository()

    fun login(email: String, password:String){
        repository.login(email, password, object : CallbackApi{
            override fun onSuccess(item: String) {

                Log.d("TAG", "onResponse: $item")
            }

            override fun onFailure(item: ResponseBody?) {
                TODO("Not yet implemented")
            }

        })
    }
}