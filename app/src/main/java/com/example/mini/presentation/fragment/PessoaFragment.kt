package com.example.mini.presentation.fragment

import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mini.R
import com.example.mini.presentation.adapter.AdapterPessoa
import com.example.mini.presentation.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.fragment_pessoa.view.*

class PessoaFragment : Fragment() {
    lateinit var viewModel: LoginViewModel
    val adapter by lazy { AdapterPessoa() }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_pessoa, container, false)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        viewModel.login("testeapple@ioasys.com.br", "12341234")

        val recyclerView = view.recyclermain
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        setHasOptionsMenu(true)

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu,menu)

        val search = menu.findItem(R.id.id_search)
        val searchView = search.actionView as androidx.appcompat.widget.SearchView
        searchView.queryHint = "Digite aqui"

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener, androidx.appcompat.widget.SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return true
            }

        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.id_search) {

        }
        return super.onOptionsItemSelected(item)
    }
}