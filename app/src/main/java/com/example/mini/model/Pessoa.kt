package com.example.mini.api.model

data class Pessoa(var nome: String, var idade: Int, var cidade: String)
